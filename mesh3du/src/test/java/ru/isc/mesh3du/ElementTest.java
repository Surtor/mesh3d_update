package ru.isc.mesh3du;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ElementTest extends TestCase {
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ElementTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ElementTest.class );
    }
    
    public void testElementNeighboringTrue()
    {
        Element a = new ElementImp(1);
        Element b = new ElementImp(2);
        NodeImp first = new NodeImp(1,1,2,3);
        NodeImp second = new NodeImp(2,3,4,5);
        NodeImp third = new NodeImp(3,6,7,8);
        NodeImp fourth = new NodeImp(4,9,10,11);
        NodeImp fifth = new NodeImp(5,12,13,14);
        
        a.addNode(first);
        a.addNode(second);
        a.addNode(third);
        a.addNode(fourth);
        
    	b.addNode(first);
    	b.addNode(second);
    	b.addNode(third);
    	b.addNode(fifth);
        
    	assertTrue(a.isNeighbor(b));
    }
    
    public void testElementNeighboringFalse()
    {
        Element a = new ElementImp(1);
        Element b = new ElementImp(2);
        NodeImp first = new NodeImp(1,1,2,3);
        NodeImp second = new NodeImp(2,3,4,5);
        NodeImp third = new NodeImp(3,6,7,8);
        NodeImp fourth = new NodeImp(4,9,10,11);
        NodeImp fifth = new NodeImp(5,12,13,14);
        NodeImp sixth = new NodeImp(6,15,16,17);
                
        a.addNode(first);
        a.addNode(second);
        a.addNode(third);
        a.addNode(fourth);
        
    	b.addNode(first);
    	b.addNode(second);
    	b.addNode(fifth);
    	b.addNode(sixth);
        
    	assertFalse(a.isNeighbor(b));
    }
}
