package ru.isc.mesh3du;

import java.util.Iterator;
import java.util.NavigableMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;

public class Mesh {
	private NavigableMap<Long, Element> mesh = new ConcurrentSkipListMap<>();
	private SortedSet<Node> nodeSet = new TreeSet<>(
			(Node a, Node b) -> Long.compare(a.getId(), b.getId()));

	private volatile long maxElId = 0;
	private volatile long maxNdId = 0;

	public long getMaxElId() {
		return maxElId;
	}

	public long getMaxNdId() {
		return maxNdId;
	}

	public synchronized Element addElement(Element el) {
		if (el.getId() > maxElId) {
			maxElId = el.getId();
		}
		Iterator<Node> ndIterator = el.getNodeIterator();

		while (ndIterator.hasNext()) {
			Node nd = ndIterator.next();
			if (nd.getId() > maxNdId) {
				maxNdId = nd.getId();
			}
			if (!nodeSet.contains(nd)) {
				nodeSet.add(nd);
			}
		}

		return mesh.put(el.getId(), el);
	}

	public synchronized boolean addNode(Node nd) {
		if (nd.getId() > maxNdId) {
			maxNdId = nd.getId();
		}
		return nodeSet.add(nd);
	}

	public synchronized boolean addNewNode(Node nd) {
		nd.setId(maxNdId++);
		return nodeSet.add(nd);
	}

	public synchronized Element addNewElement(Element el) {
		Element newElement = el.clone();
		newElement.setId(maxElId + 1);
		return addElement(newElement);
	}

	public synchronized Iterator<Element> getElementIterator() {
		return mesh.values().iterator();
	}

	public Element getElement(long elId) {
		return mesh.get(elId);
	}

	public synchronized Iterator<Element> getTail(long elId) {
		return mesh.tailMap(elId, false).values().iterator();
	}

	public boolean removeElement(Element el) {
		return mesh.remove(el.getId(), el);
	}

	public Element removeElement(long elId) {
		return mesh.remove(elId);
	}

	@Override
	public String toString() {
		return "Mesh [mesh=" + mesh + "]";
	}

	public synchronized Iterator<Node> getNodeIterator() {
		/*
		 * Iterator<Element> it = mesh.values().iterator(); SortedSet<Node>
		 * nodeSet = new TreeSet<>( (Node a, Node b) -> Long.compare(a.getId(),
		 * b.getId()));
		 */

		/*
		 * it.forEachRemaining(value1 -> value1.getNodeIterator()
		 * .forEachRemaining(value2 -> nodeSet.add(value2)));
		 */
		return nodeSet.iterator();

	}
}
