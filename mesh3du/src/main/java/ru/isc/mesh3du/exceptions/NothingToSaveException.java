package ru.isc.mesh3du.exceptions;

public class NothingToSaveException extends Mesh3dUExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8607818756011160266L;

	public NothingToSaveException() {
		// TODO Auto-generated constructor stub
	}

	public NothingToSaveException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NothingToSaveException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NothingToSaveException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NothingToSaveException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getErrorMessage() {
		return new String("Nothing to save!");
	}

}
