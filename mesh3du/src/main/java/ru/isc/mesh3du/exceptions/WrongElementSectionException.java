package ru.isc.mesh3du.exceptions;

public class WrongElementSectionException extends Mesh3dUExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5452611127917093014L;

	public WrongElementSectionException() {
		// TODO Auto-generated constructor stub
	}

	public WrongElementSectionException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public WrongElementSectionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public WrongElementSectionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WrongElementSectionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return new String("Wrong Element section!");
	}

}
