package ru.isc.mesh3du.exceptions;

public class WrongNodeSectionException extends Mesh3dUExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 110440464238569197L;

	public WrongNodeSectionException() {
		// TODO Auto-generated constructor stub
	}

	public WrongNodeSectionException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public WrongNodeSectionException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public WrongNodeSectionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public WrongNodeSectionException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getErrorMessage() {
		return new String("Wrong Node section!");
	}

}
