package ru.isc.mesh3du.exceptions;

public class NoNodeSectionException extends Mesh3dUExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5754001310876330778L;

	public NoNodeSectionException() {
		// TODO Auto-generated constructor stub
	}

	public NoNodeSectionException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NoNodeSectionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NoNodeSectionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoNodeSectionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return new String("Node section doesn't exist!");
	}

}
