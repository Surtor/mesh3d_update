package ru.isc.mesh3du.exceptions;

@SuppressWarnings("serial")
public abstract class Mesh3dUExceptions extends Exception {

	abstract public String getErrorMessage();

	public Mesh3dUExceptions() {
		// TODO Auto-generated constructor stub
	}

	public Mesh3dUExceptions(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public Mesh3dUExceptions(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public Mesh3dUExceptions(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public Mesh3dUExceptions(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
