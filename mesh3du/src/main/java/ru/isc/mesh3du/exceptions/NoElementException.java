package ru.isc.mesh3du.exceptions;

public class NoElementException extends Mesh3dUExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5618506250072018372L;

	public NoElementException() {
		// TODO Auto-generated constructor stub
	}

	public NoElementException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NoElementException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NoElementException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public NoElementException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getErrorMessage() {

		return new String("Element section doesn't exist!");
	}

}
