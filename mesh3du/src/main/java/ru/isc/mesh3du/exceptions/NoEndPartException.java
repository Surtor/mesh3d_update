package ru.isc.mesh3du.exceptions;

public class NoEndPartException extends Mesh3dUExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 633324582921447479L;

	public NoEndPartException() {
		// TODO Auto-generated constructor stub
	}

	public NoEndPartException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NoEndPartException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NoEndPartException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoEndPartException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getErrorMessage() {
		return new String("End part doesn't exist!");
	}

}
