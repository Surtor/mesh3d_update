package ru.isc.mesh3du;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

import ru.isc.mesh3du.exceptions.NoElementException;
import ru.isc.mesh3du.exceptions.NoEndPartException;
import ru.isc.mesh3du.exceptions.NoNodeSectionException;
import ru.isc.mesh3du.exceptions.NothingToSaveException;
import ru.isc.mesh3du.exceptions.WrongElementSectionException;
import ru.isc.mesh3du.exceptions.WrongNodeSectionException;

public class MeshFile {
	private Mesh mesh = null;
	private StringBuilder header = null;
	private StringBuilder bottom = null;
	private boolean isLoad = false;

	/* вспомогательные поля */
	private Scanner inFileScanner = null;
	private HashMap<Long, Node> nodes = null;

	private StringBuilder getHeader() throws NoNodeSectionException {
		Pattern nodeSectionPattern = Pattern.compile("\\s*\\*Node\\s*");
		StringBuilder returnHeader = new StringBuilder();
		while (inFileScanner.hasNext()) {
			if (!inFileScanner.hasNext(nodeSectionPattern)) {
				returnHeader.append(inFileScanner.nextLine()).append("\n");
			} else {
				return returnHeader;
			}
		}
		throw new NoNodeSectionException();
	}

	private HashMap<Long, Node> getNodes() throws WrongNodeSectionException,
			NoElementException {
		HashMap<Long, Node> returnNodes = new HashMap<>();
		inFileScanner.nextLine();

		while (inFileScanner.hasNext()) {
			if (!inFileScanner.hasNext("\\*Element.*")) {
				Scanner newLineScanner = new Scanner(inFileScanner.nextLine());
				newLineScanner.useDelimiter("\\s*\\,\\s*");
				newLineScanner.skip("\\s*");
				long id;
				double X;
				double Y;
				double Z;
				try {
					id = Long.parseLong(newLineScanner.next());
					X = Double.parseDouble(newLineScanner.next());
					Y = Double.parseDouble(newLineScanner.next());
					Z = Double.parseDouble(newLineScanner.next());
					returnNodes.put(id, new NodeImp(id, X, Y, Z));
				} catch (NumberFormatException e) {
					returnNodes = null;
					throw new WrongNodeSectionException();

				} finally {
					newLineScanner.close();
				}
			} else {
				return returnNodes;
			}
		}
		throw new NoElementException();

	}

	private void getMeshFromFile() throws NoElementException,
			WrongElementSectionException {
		if (!inFileScanner.hasNext("\\s*\\*Element,\\s*"))
			throw new NoElementException();

		inFileScanner.next();
		if (!inFileScanner.hasNext("\\s*type=\\S*\\s*"))
			throw new WrongElementSectionException();

		Scanner newLineScanner = new Scanner(inFileScanner.nextLine());
		String type = newLineScanner.findInLine("type=\\S+").substring(5);
		newLineScanner.close();
		while (inFileScanner.hasNext()) {
			if (inFileScanner.hasNext("\\s*\\*End\\s*")) {
				break;
			}
			if (inFileScanner.hasNext("\\s*\\*Element,\\s*")) {
				getMeshFromFile();
				break;
			}
			Element newElement = new ElementImp();
			newElement.setType(type);
			newLineScanner = new Scanner(inFileScanner.nextLine());
			newLineScanner.useDelimiter("\\s*\\,\\s*");
			newLineScanner.skip("\\s*");
			long elementId;

			try {
				elementId = Long.parseLong(newLineScanner.next());
			} catch (NumberFormatException e) {
				newLineScanner.close();
				throw new WrongElementSectionException();
			}
			newElement.setId(elementId);

			if (!newLineScanner.hasNext()) {
				newLineScanner.close();
				throw new WrongElementSectionException();
			}

			while (newLineScanner.hasNext()) {
				long nodeId;
				try {
					nodeId = Long.parseLong(newLineScanner.next());
				} catch (NumberFormatException e) {
					newLineScanner.close();
					throw new WrongElementSectionException();
				}
				newElement.addNode(nodes.get(nodeId));
			}
			newLineScanner.close();
			mesh.addElement(newElement);
		}
	}

	private StringBuilder getBottom() throws NoEndPartException {
		StringBuilder returnBottom = new StringBuilder();
		if (!inFileScanner.hasNext("\\s*\\*End\\s*")) {
			throw new NoEndPartException();
		}
		returnBottom.append(inFileScanner.next());

		if (!inFileScanner.hasNext("\\s*Part\\s*")) {
			throw new NoEndPartException();
		}
		returnBottom.append(inFileScanner.nextLine()).append("\n");

		while (inFileScanner.hasNext()) {
			returnBottom.append(inFileScanner.nextLine()).append("\n");

		}
		return returnBottom;
	}

	public void loadFromFile(String fileName) throws FileNotFoundException,
			NoNodeSectionException, WrongNodeSectionException,
			NoElementException, WrongElementSectionException,
			NoEndPartException {
		inFileScanner = new Scanner(new FileInputStream(fileName));
		header = getHeader();
		nodes = getNodes();
		mesh = new Mesh();
		getMeshFromFile();
		bottom = getBottom();

		inFileScanner.close();
		if ((header != null) && (nodes != null) && (bottom != null)) {
			isLoad = true;
			return;
		}
		header = null;
		nodes = null;
		mesh = null;
		bottom = null;
	}

	private void printNodes(PrintWriter oFile) {
		Iterator<Node> ndIterator = mesh.getNodeIterator();
		oFile.println("*Node");
		while (ndIterator.hasNext()) {
			Node nd = ndIterator.next();
			oFile.printf(Locale.US, "%7d,%16.11f,%16.11f,%16.11f\n",
					nd.getId(), nd.getX(), nd.getY(), nd.getZ());
		}
	}

	private void printElements(PrintWriter oFile) {
		ArrayList<Element> elArrlist = new ArrayList<>();
		Iterator<Element> elIterator = mesh.getElementIterator();
		while (elIterator.hasNext()) {
			elArrlist.add(elIterator.next());
		}
		Collections.sort(elArrlist, (Element el1, Element el2) -> el1.getType()
				.compareTo(el2.getType()));

		elIterator = elArrlist.iterator();
		/*
		 * if(elIterator.hasNext()){
		 * 
		 * }
		 */
		String type = null;
		while (elIterator.hasNext()) {
			Element el = elIterator.next();
			if (!el.getType().equals(type)) {
				oFile.printf("*Element, type=%s\n", el.getType());
				type = el.getType();
			}
			Iterator<Node> ndIterator = el.getNodeIterator();
			oFile.printf("%7d,", el.getId());
			while (ndIterator.hasNext()) {
				oFile.printf("%7d", ndIterator.next().getId());
				if (ndIterator.hasNext()) {
					oFile.print(",");
				} else {
					oFile.print("\n");
				}
			}
		}
	}

	public void saveToFile(String fileName) throws FileNotFoundException,
			NothingToSaveException {
		if (!isLoad)
			throw new NothingToSaveException();
		PrintWriter outFile = new PrintWriter(fileName);
		outFile.print(header.toString());
		printNodes(outFile);
		printElements(outFile);
		outFile.print(bottom.toString());
		outFile.close();

	}

	public Mesh getMesh() {
		return mesh;
	}

	public MeshFile() {

	}

	public MeshFile(String fileName) throws FileNotFoundException,
			NoNodeSectionException, WrongNodeSectionException,
			NoElementException, WrongElementSectionException,
			NoEndPartException {
		loadFromFile(fileName);
	}

	public void setBottom(String bt) {
		this.bottom = new StringBuilder(bt);
	}
}
