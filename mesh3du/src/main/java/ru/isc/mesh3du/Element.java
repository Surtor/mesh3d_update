package ru.isc.mesh3du;

import java.util.Iterator;

public interface Element {

	public abstract boolean isNeighbor(Element el);

	public abstract void clearAllNodes();

	public abstract boolean addNode(Node addedNode);

	public abstract Node setNode(long ndId, Node nd);

	public abstract Iterator<Node> getNodeIterator();

	public abstract long getId();

	public abstract void setId(long id);

	public abstract Node getNode(long ndId);

	public abstract Element clone();

	public abstract String getType();

	public abstract void setType(String type);

	public abstract int quantityNodes();

}