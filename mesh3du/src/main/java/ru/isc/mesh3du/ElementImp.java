package ru.isc.mesh3du;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ElementImp implements Element, Cloneable {
	private long id;
	private String type;
	private List<Node> nodeList = new ArrayList<>();

	private long addingElementToSet(Set<Node> set, Iterator<Node> it) {
		long qauntityOfAddedElements = 0;
		if ((set == null) || (it == null))
			return qauntityOfAddedElements;
		while (it.hasNext()) {
			set.add(it.next());
			qauntityOfAddedElements++;
		}
		return qauntityOfAddedElements;
	}

	@Override
	public Node getNode(long ndId) {
		return nodeList.get((int) ndId);
	}

	@Override
	public String toString() {
		return "ElementImp [id=" + id + ", type=" + type + ", nodeList="
				+ nodeList + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Element#isNeighbor(ru.isc.mesh3du.Element)
	 */
	@Override
	public boolean isNeighbor(Element el) {
		if (this.equals(el))
			return false;
		Set<Node> thisSet = new HashSet<>();
		Set<Node> elSet = new HashSet<>();
		addingElementToSet(thisSet, this.getNodeIterator());
		addingElementToSet(elSet, el.getNodeIterator());
		thisSet.retainAll(elSet);

		if (thisSet.size() > 2 && elSet.size() <= 6)
			return true;

		if (thisSet.size() > 3)
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result
				+ ((nodeList == null) ? 0 : nodeList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElementImp other = (ElementImp) obj;
		if (id != other.id)
			return false;
		if (nodeList == null) {
			if (other.nodeList != null)
				return false;
		} else if (!nodeList.equals(other.nodeList))
			return false;
		return true;
	}

	@Override
	public Element clone() {
		Element returnEl = new ElementImp(this.getId());
		Iterator<Node> it = this.getNodeIterator();
		while (it.hasNext()) {
			returnEl.addNode(new NodeImp(it.next()));
		}
		return returnEl;
	}

	public ElementImp(long id, Node... nodeList) {
		super();
		this.id = id;
		for (Node i : nodeList)
			this.nodeList.add(i);

	}

	public ElementImp() {
		super();
	}

	public ElementImp(Element el) {
		this.id = el.getId();
		Iterator<Node> it = el.getNodeIterator();

		while (it.hasNext()) {
			this.nodeList.add(it.next().clone());
		}
	}

	@Override
	public Node setNode(long ndId, Node nd) {
		for (int i = 0; i < nodeList.size(); i++) {
			if (nodeList.get(i).getId() == ndId) {
				nodeList.set(i, nd);
				return nd;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Element#clearAllNodes()
	 */
	@Override
	public void clearAllNodes() {
		nodeList.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Element#addNode(ru.isc.mesh3du.NodeImp)
	 */
	@Override
	public boolean addNode(Node addedNode) {
		return nodeList.add(addedNode);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Element#getNodeIterator()
	 */
	@Override
	public Iterator<Node> getNodeIterator() {
		return nodeList.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Element#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Element#setId(long)
	 */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int quantityNodes() {
		return nodeList.size();
	}

}
