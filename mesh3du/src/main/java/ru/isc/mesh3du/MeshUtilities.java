package ru.isc.mesh3du;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Utilities for mesh.
 * 
 * @author Surtor
 */
public class MeshUtilities {

	/**
	 * Divide all elements in mesh.
	 * 
	 * @param m
	 *            mesh to dividing.
	 */
	public static void divideAllElements(Mesh m) {
		Iterator<Element> el1Iterator = m.getElementIterator();
		while (el1Iterator.hasNext()) {
			Element el1 = el1Iterator.next();
			System.out.println(el1.getId());
			Iterator<Element> el2Iterator = m.getTail(el1.getId());
			// Iterator<Element> el2Iterator = m.getElementIterator();
			while (el2Iterator.hasNext()) {
				Element el2 = el2Iterator.next();
				Iterator<Node> el1NodeIterator = el1.getNodeIterator();
				while (el1NodeIterator.hasNext()) {
					Iterator<Node> el2NodeIterator = el2.getNodeIterator();
					Node nd1 = el1NodeIterator.next();
					/*
					 * System.out.println(el1.getId() + " : " + nd1);
					 * System.out.println();
					 */
					while (el2NodeIterator.hasNext()) {
						Node nd2 = el2NodeIterator.next();
						if (nd1.getId() == nd2.getId()) {

							/*
							 * System.out.println(el1.getId() + " : " + nd1);
							 * System.out.println(el2.getId() + " : " + nd2);
							 * System.out.println();
							 */
							Node newNode = new NodeImp(nd1);
							newNode.setId(m.getMaxNdId() + 1);
							// m.addNewNode(newNode);
							m.addNode(newNode);
							el1.setNode(nd1.getId(), newNode);

						}
					}
				}
			}
		}
	}

	/*
	 * public static void divideAllElements2(Mesh m){ for() }
	 */

	// depricated
	public static void shrinkWedge(Mesh m) {
		divideAllElements(m);
		Iterator<Element> elIterator = m.getElementIterator();

		HashSet<Node> firstTriangle = new HashSet<>();
		HashSet<Node> secondTriangle;

		while (elIterator.hasNext()) {
			secondTriangle = new HashSet<>();
			Element el = elIterator.next();
			Iterator<Node> ndIterator1 = el.getNodeIterator();
			if (ndIterator1.hasNext()) {
				Node ndOrigin = ndIterator1.next();

				HashSet<Node> xS = new HashSet<>();
				HashSet<Node> yS = new HashSet<>();
				HashSet<Node> zS = new HashSet<>();

				secondTriangle.add(ndOrigin);

				while (ndIterator1.hasNext()) {

					Node nd2 = ndIterator1.next();
					secondTriangle.add(nd2);

					if (ndOrigin.getX() == nd2.getX()) {
						xS.add(nd2);
					}
					if (ndOrigin.getY() == nd2.getY()) {
						yS.add(nd2);
					}
					if (ndOrigin.getZ() == nd2.getZ()) {
						zS.add(nd2);
					}
				}
				if (xS.size() == 2) {
					xS.add(ndOrigin);
					firstTriangle = xS;
				} else if (yS.size() == 2) {
					yS.add(ndOrigin);
					firstTriangle = yS;
				} else if (zS.size() == 2) {
					zS.add(ndOrigin);
					firstTriangle = zS;
				}
				secondTriangle.removeAll(firstTriangle);
				shrinkTriangle(firstTriangle, 0.1);
				shrinkTriangle(secondTriangle, 0.1);
			}
		}
	}

	public static void shrinkTetraider(Mesh m, double coeff) {
		divideAllElements(m);
		Iterator<Element> elIterator = m.getElementIterator();
		while (elIterator.hasNext()) {
			Element el = elIterator.next();
			Iterator<Node> ndIt = el.getNodeIterator();
			double xC = 0, yC = 0, zC = 0;
			int qOfN = 0;
			while (ndIt.hasNext()) {
				Node nd = ndIt.next();
				qOfN++;
				xC += nd.getX();
				yC += nd.getY();
				zC += nd.getZ();
			}

			xC /= qOfN;
			yC /= qOfN;
			zC /= qOfN;

			ndIt = el.getNodeIterator();
			double tempX, tempY, tempZ;
			double dx, dy, dz;
			while (ndIt.hasNext()) {
				Node nd = ndIt.next();
				dx = Math.abs(nd.getX() - xC) * coeff;
				dy = Math.abs(nd.getY() - yC) * coeff;
				dz = Math.abs(nd.getZ() - zC) * coeff;

				if ((nd.getX() - xC) <= 0) {
					tempX = nd.getX() + dx;
				} else {
					tempX = nd.getX() - dx;
				}

				if ((nd.getY() - yC) <= 0) {
					tempY = nd.getY() + dy;
				} else {
					tempY = nd.getY() - dy;
				}

				if ((nd.getZ() - zC) <= 0) {
					tempZ = nd.getZ() + dz;
				} else {
					tempZ = nd.getZ() - dz;
				}
				nd.setX(tempX);
				nd.setY(tempY);
				nd.setZ(tempZ);
			}

		}
	}

	public static void shrinkTetraiderTypeEl(Mesh m, double coeff, String type) {
		Iterator<Element> elIterator = m.getElementIterator();
		while (elIterator.hasNext()) {
			Element el = elIterator.next();
			if (!el.getType().equals(type)) {
				continue;
			}
			Iterator<Node> ndIt = el.getNodeIterator();
			double xC = 0, yC = 0, zC = 0;
			int qOfN = 0;
			while (ndIt.hasNext()) {
				Node nd = ndIt.next();
				qOfN++;
				xC += nd.getX();
				yC += nd.getY();
				zC += nd.getZ();
			}

			xC /= qOfN;
			yC /= qOfN;
			zC /= qOfN;

			ndIt = el.getNodeIterator();
			double tempX, tempY, tempZ;
			double dx, dy, dz;
			while (ndIt.hasNext()) {
				Node nd = ndIt.next();
				dx = Math.abs(nd.getX() - xC) * coeff;
				dy = Math.abs(nd.getY() - yC) * coeff;
				dz = Math.abs(nd.getZ() - zC) * coeff;

				if ((nd.getX() - xC) <= 0) {
					tempX = nd.getX() + dx;
				} else {
					tempX = nd.getX() - dx;
				}

				if ((nd.getY() - yC) <= 0) {
					tempY = nd.getY() + dy;
				} else {
					tempY = nd.getY() - dy;
				}

				if ((nd.getZ() - zC) <= 0) {
					tempZ = nd.getZ() + dz;
				} else {
					tempZ = nd.getZ() - dz;
				}
				nd.setX(tempX);
				nd.setY(tempY);
				nd.setZ(tempZ);
			}

		}
	}

	private static void shrinkTriangle(HashSet<Node> triangle, double coeff) {
		// Находим центр треугольника в трехмерном пространстве
		double xC = 0, yC = 0, zC = 0;
		Iterator<Node> ndIt = triangle.iterator();

		while (ndIt.hasNext()) {
			Node nd = ndIt.next();
			xC += nd.getX();
			yC += nd.getY();
			zC += nd.getZ();
		}
		xC /= 3;
		yC /= 3;
		zC /= 3;

		ndIt = triangle.iterator();
		double tempX, tempY, tempZ;
		double dx, dy, dz;
		while (ndIt.hasNext()) {
			Node nd = ndIt.next();
			dx = Math.abs(nd.getX() - xC) * coeff;
			dy = Math.abs(nd.getY() - yC) * coeff;
			dz = Math.abs(nd.getZ() - zC) * coeff;

			if ((nd.getX() - xC) <= 0) {
				tempX = nd.getX() + dx;
			} else {
				tempX = nd.getX() - dx;
			}

			if ((nd.getY() - yC) <= 0) {
				tempY = nd.getY() + dy;
			} else {
				tempY = nd.getY() - dy;
			}

			if ((nd.getZ() - zC) <= 0) {
				tempZ = nd.getZ() + dz;
			} else {
				tempZ = nd.getZ() - dz;
			}
			nd.setX(tempX);
			nd.setY(tempY);
			nd.setZ(tempZ);
		}
	}

	private static Map<Long, Set<Element>> returnConnectivityMatrix(Mesh m) {
		Map<Long, Set<Element>> returnMap = new TreeMap<>();
		Iterator<Element> itEl1 = m.getElementIterator();
		while (itEl1.hasNext()) {
			Element el1 = itEl1.next();
			Iterator<Element> itEl2 = m.getElementIterator();
			while (itEl2.hasNext()) {
				Element el2 = itEl2.next();
				if (el1.isNeighbor(el2)) {
					if (!returnMap.containsKey(el1.getId())) {
						returnMap.put(el1.getId(), new HashSet<Element>());
					}
					if (!returnMap.containsKey(el2.getId())) {
						returnMap.put(el2.getId(), new HashSet<Element>());
					}
					returnMap.get(el1.getId()).add(el2);
					returnMap.get(el2.getId()).add(el1);
				}

			}
		}
		return returnMap;
	}

	private static ArrayList<ConnectivityFaces> returnConnectivityFacesWedge(
			Map<Long, Set<Element>> f, Mesh m) {
		ArrayList<ConnectivityFaces> connectivityF = new ArrayList<>();
		Set<Long> allKeys = f.keySet();
		for (Long keysOfElements : allKeys) {
			Element el1 = m.getElement(keysOfElements);
			Face[] el1FaceArray = getFacesFromElementWedge(el1);
			for (Element el2 : f.get(keysOfElements)) {
				Face[] el2FaceArray = getFacesFromElementWedge(el2);
				exitFaceCompare: for (Face fc1Element : el1FaceArray) {
					for (Face fc2Element : el2FaceArray) {
						if (fc1Element.isNeighbor(fc2Element)) {
							connectivityF.add(new ConnectivityFaces(fc1Element,
									fc2Element));
							f.get(el2.getId()).remove(el1);
							break exitFaceCompare;
						}
					}
				}

			}
		}
		return connectivityF;
	}

	private static ArrayList<ConnectivityFaces> returnConnectivityFacesSecondTetra(
			Map<Long, Set<Element>> f, Mesh m) {
		ArrayList<ConnectivityFaces> connectivityF = new ArrayList<>();
		Set<Long> allKeys = f.keySet();
		for (Long keysOfElements : allKeys) {
			Element el1 = m.getElement(keysOfElements);
			Face[] el1FaceArray = getFacesFromElementTetra(el1);
			for (Element el2 : f.get(keysOfElements)) {
				Face[] el2FaceArray = getFacesFromElementTetra(el2);
				exitFaceCompare: for (Face fc1Element : el1FaceArray) {
					for (Face fc2Element : el2FaceArray) {
						if (fc1Element.isNeighbor(fc2Element)) {
							connectivityF.add(new ConnectivityFaces(fc1Element,
									fc2Element));
							f.get(el2.getId()).remove(el1);
							break exitFaceCompare;
						}
					}
				}

			}
		}
		return connectivityF;
	}

	private static Face[] getFacesFromElementTetra(Element el) {

		Face[] ArrFace = new Face[4];

		Face temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(1);
		temp.addNode(el.getNode(0));
		temp.addNode(el.getNode(4));
		temp.addNode(el.getNode(1));
		temp.addNode(el.getNode(5));
		temp.addNode(el.getNode(2));
		temp.addNode(el.getNode(6));
		ArrFace[0] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(2);
		temp.addNode(el.getNode(0));
		temp.addNode(el.getNode(7));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(8));
		temp.addNode(el.getNode(1));
		temp.addNode(el.getNode(4));
		ArrFace[1] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(3);
		temp.addNode(el.getNode(1));
		temp.addNode(el.getNode(8));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(9));
		temp.addNode(el.getNode(2));
		temp.addNode(el.getNode(5));
		ArrFace[2] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(4);
		temp.addNode(el.getNode(2));
		temp.addNode(el.getNode(9));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(7));
		temp.addNode(el.getNode(0));
		temp.addNode(el.getNode(6));
		ArrFace[3] = temp;

		return ArrFace;

	}

	private static Face[] getFacesFromElementWedge(Element el) {

		Face[] ArrFace = new Face[3];

		Face temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(3);
		temp.addNode(el.getNode(0));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(4));
		temp.addNode(el.getNode(1));
		ArrFace[0] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(4);
		temp.addNode(el.getNode(1));
		temp.addNode(el.getNode(4));
		temp.addNode(el.getNode(5));
		temp.addNode(el.getNode(2));
		ArrFace[1] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(5);
		temp.addNode(el.getNode(2));
		temp.addNode(el.getNode(5));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(0));
		ArrFace[2] = temp;

		return ArrFace;

	}

	private static void updateFaceWedge(Face fc, Mesh m) {
		int elementId = fc.getElementId();
		fc.clearAllNodes();
		if (fc.getElementFaceId() == 3) {
			fc.addNode(m.getElement(elementId).getNode(0));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(4));
			fc.addNode(m.getElement(elementId).getNode(1));
			return;
		}
		if (fc.getElementFaceId() == 4) {
			fc.addNode(m.getElement(elementId).getNode(1));
			fc.addNode(m.getElement(elementId).getNode(4));
			fc.addNode(m.getElement(elementId).getNode(5));
			fc.addNode(m.getElement(elementId).getNode(2));
			return;
		}
		if (fc.getElementFaceId() == 5) {
			fc.addNode(m.getElement(elementId).getNode(2));
			fc.addNode(m.getElement(elementId).getNode(5));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(0));
			return;
		}
	}

	private static void updateConnectivityFacesWedge(
			List<ConnectivityFaces> connectivityF, Mesh m) {
		for (ConnectivityFaces cF : connectivityF) {
			updateFaceWedge(cF.getFace1(), m);
			updateFaceWedge(cF.getFace2(), m);
		}
	}

	private static void createCohesive(List<ConnectivityFaces> f, Mesh m,
			String type) {
		for (ConnectivityFaces cf : f) {
			Face fc1 = cf.getFace1();
			Face fc2 = cf.getFace2();
			Element newElement = new ElementImp();
			for (Node nd : fc1) {
				newElement.addNode(nd);
			}

			for (Node nd1 : fc1) {
				for (Node nd2 : fc2) {
					if (nd1.isSameCoordinates(nd2)) {
						newElement.addNode(nd2);
						break;
					}
				}
			}
			newElement.setType(type/* "COH3D8" */);
			// m.addNewElement(newElement);
			newElement.setId(m.getMaxElId() + 1);
			m.addElement(newElement);

		}
	}

	/**
	 * Добавляем кохесив элементы в клинья
	 * 
	 * @param m
	 */
	public static void addCohesiveWedge(Mesh m) {
		// создать структуру со смежными элементами
		// Map

		// создать структуру со смежными поверхностями

		// разделить элементы и смежные поверхности

		// вставить кохесив элементы с использованием структуры со смежными
		// поверхностями
		Map<Long, Set<Element>> connectivityMap = returnConnectivityMatrix(m);
		List<ConnectivityFaces> connectivityF = returnConnectivityFacesWedge(
				connectivityMap, m);
		divideAllElements(m);
		updateConnectivityFacesWedge(connectivityF, m);
		createCohesive(connectivityF, m, "COH3D8");

		/*
		 * for (ConnectivityFaces tempCF : connectivityF) {
		 * System.out.println(tempCF); }
		 */

		/*
		 * Set<Long> setEl = rm.keySet();
		 * 
		 * for (long l : setEl) { System.out.println("Element " + l +
		 * " имеет смежные элементы:"); for (Element elIt : rm.get(l)) {
		 * System.out.println(elIt); } System.out.println(""); }
		 */
	}

	private static Face[] getFacesFromElementTetraider(Element el) {

		Face[] ArrFace = new Face[4];

		Face temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(1);
		temp.addNode(el.getNode(0));
		temp.addNode(el.getNode(1));
		temp.addNode(el.getNode(2));
		ArrFace[0] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(2);
		temp.addNode(el.getNode(0));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(1));
		ArrFace[1] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(3);
		temp.addNode(el.getNode(1));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(2));
		ArrFace[2] = temp;

		temp = new Face();
		temp.setElementId((int) el.getId());
		temp.setElementFaceId(4);
		temp.addNode(el.getNode(2));
		temp.addNode(el.getNode(3));
		temp.addNode(el.getNode(0));
		ArrFace[3] = temp;

		return ArrFace;

	}

	private static void updateFaceTetraider(Face fc, Mesh m) {
		int elementId = fc.getElementId();
		fc.clearAllNodes();
		if (fc.getElementFaceId() == 1) {
			fc.addNode(m.getElement(elementId).getNode(0));
			fc.addNode(m.getElement(elementId).getNode(1));
			fc.addNode(m.getElement(elementId).getNode(2));
			return;
		}
		if (fc.getElementFaceId() == 2) {
			fc.addNode(m.getElement(elementId).getNode(0));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(1));
			return;
		}
		if (fc.getElementFaceId() == 3) {
			fc.addNode(m.getElement(elementId).getNode(1));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(2));
			return;
		}
		if (fc.getElementFaceId() == 4) {
			fc.addNode(m.getElement(elementId).getNode(2));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(0));
			return;
		}
	}

	private static void updateFaceSecondTetra(Face fc, Mesh m) {
		int elementId = fc.getElementId();
		fc.clearAllNodes();
		if (fc.getElementFaceId() == 1) {
			fc.addNode(m.getElement(elementId).getNode(0));
			fc.addNode(m.getElement(elementId).getNode(4));
			fc.addNode(m.getElement(elementId).getNode(1));
			fc.addNode(m.getElement(elementId).getNode(5));
			fc.addNode(m.getElement(elementId).getNode(2));
			fc.addNode(m.getElement(elementId).getNode(6));
			return;
		}
		if (fc.getElementFaceId() == 2) {
			fc.addNode(m.getElement(elementId).getNode(0));
			fc.addNode(m.getElement(elementId).getNode(7));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(8));
			fc.addNode(m.getElement(elementId).getNode(1));
			fc.addNode(m.getElement(elementId).getNode(4));
			return;
		}
		if (fc.getElementFaceId() == 3) {
			fc.addNode(m.getElement(elementId).getNode(1));
			fc.addNode(m.getElement(elementId).getNode(8));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(9));
			fc.addNode(m.getElement(elementId).getNode(2));
			fc.addNode(m.getElement(elementId).getNode(5));
			return;
		}
		if (fc.getElementFaceId() == 4) {
			fc.addNode(m.getElement(elementId).getNode(2));
			fc.addNode(m.getElement(elementId).getNode(9));
			fc.addNode(m.getElement(elementId).getNode(3));
			fc.addNode(m.getElement(elementId).getNode(7));
			fc.addNode(m.getElement(elementId).getNode(0));
			fc.addNode(m.getElement(elementId).getNode(6));
			return;
		}
	}

	private static ArrayList<ConnectivityFaces> returnConnectivityFacesTetraider(
			Map<Long, Set<Element>> f, Mesh m) {
		ArrayList<ConnectivityFaces> connectivityF = new ArrayList<>();
		Set<Long> allKeys = f.keySet();
		for (Long keysOfElements : allKeys) {
			Element el1 = m.getElement(keysOfElements);
			Face[] el1FaceArray = getFacesFromElementTetraider(el1);
			for (Element el2 : f.get(keysOfElements)) {
				Face[] el2FaceArray = getFacesFromElementTetraider(el2);
				exitFaceCompare: for (Face fc1Element : el1FaceArray) {
					for (Face fc2Element : el2FaceArray) {
						if (fc1Element.isNeighbor(fc2Element)) {
							connectivityF.add(new ConnectivityFaces(fc1Element,
									fc2Element));
							f.get(el2.getId()).remove(el1);
							break exitFaceCompare;
						}
					}
				}

			}
		}
		return connectivityF;
	}

	private static void updateConnectivityFacesTetraider(
			List<ConnectivityFaces> connectivityF, Mesh m) {
		for (ConnectivityFaces cF : connectivityF) {
			updateFaceTetraider(cF.getFace1(), m);
			updateFaceTetraider(cF.getFace2(), m);
		}
	}

	private static void updateConnectivityFacesSecondTetra(
			List<ConnectivityFaces> connectivityF, Mesh m) {
		for (ConnectivityFaces cF : connectivityF) {
			updateFaceSecondTetra(cF.getFace1(), m);
			updateFaceSecondTetra(cF.getFace2(), m);
		}
	}

	public static void addCohesiveTetraider(Mesh m) {
		Map<Long, Set<Element>> connectivityMap = returnConnectivityMatrix(m);
		List<ConnectivityFaces> connectivityF = returnConnectivityFacesTetraider(
				connectivityMap, m);
		divideAllElements(m);
		updateConnectivityFacesTetraider(connectivityF, m);
		createCohesive(connectivityF, m, "COH3D6");

	}

	public static void addCohesiveInteractionsTetraider(MeshFile mf) {
		Map<Long, Set<Element>> connectivityMap = returnConnectivityMatrix(mf
				.getMesh());
		List<ConnectivityFaces> connectivityF = returnConnectivityFacesTetraider(
				connectivityMap, mf.getMesh());
		divideAllElements(mf.getMesh());
		updateConnectivityFacesTetraider(connectivityF, mf.getMesh());

		String bt = toStringCohesiveInteractions(connectivityF, mf.getMesh());
		mf.setBottom(bt);
		// System.out.println(bt);
	}

	public static void addCohesiveInteractionsWedge(MeshFile mf) {
		Map<Long, Set<Element>> connectivityMap = returnConnectivityMatrix(mf
				.getMesh());
		List<ConnectivityFaces> connectivityF = returnConnectivityFacesWedge(
				connectivityMap, mf.getMesh());
		divideAllElements(mf.getMesh());
		updateConnectivityFacesTetraider(connectivityF, mf.getMesh());

		String bt = toStringCohesiveInteractions(connectivityF, mf.getMesh());
		mf.setBottom(bt);
		// System.out.println(bt);
	}

	public static void addCohesiveInteractionsSecondTetra(MeshFile mf) {
		Map<Long, Set<Element>> connectivityMap = returnConnectivityMatrix(mf
				.getMesh());
		List<ConnectivityFaces> connectivityF = returnConnectivityFacesSecondTetra(
				connectivityMap, mf.getMesh());
		divideAllElements(mf.getMesh());
		updateConnectivityFacesSecondTetra(connectivityF, mf.getMesh());

		String bt = toStringCohesiveInteractions(connectivityF, mf.getMesh());
		mf.setBottom(bt);

	}

	private static String toStringCohesiveInteractions(
			List<ConnectivityFaces> connectivityF, Mesh m) {
		StringBuilder out = new StringBuilder();
		// Начало конца
		out.append("*End Part\n" + "**\n" + "**\n" + "** ASSEMBLY\n" + "**\n"
				+ "*Assembly, name=Assembly\n" + "**\n"
				+ "*Instance, name=Part-1-1, part=Part-1\n" + "*End Instance\n"
				+ "**\n");
		Iterator<Element> it = m.getElementIterator();
		while (it.hasNext()) {
			Element el = it.next();
			out.append("*Elset, elset=El-" + el.getId()
					+ ", internal, instance=Part-1-1\n" + el.getId() + ",\n");
			int faceNumber = numberOfFaces(el.quantityNodes());
			for (int i = 0; i < faceNumber; i++) {
				out.append("*Surface, type=ELEMENT, name=El" + el.getId() + "S"
						+ (i + 1) + "\n" + "El-" + el.getId() + "," + " S"
						+ (i + 1) + "\n");
			}
			out.append("\n");
		}
		out.append("*End Assembly\n" + "**\n" + "** INTERACTION PROPERTIES\n"
				+ "**\n" + "*Surface Interaction, name=COHESION\n" +

				"*Surface Interaction, name=GENERALPROPERTIES\n" +

				"**\n" + "** INTERACTIONS\n" + "**\n"
				+ "** Interaction: GeneralContact\n" + "*Contact, op=NEW\n"
				+ "*Contact Inclusions, ALL EXTERIOR\n"
				+ "*Contact Property Assignment\n" + ",  , GENERALPROPERTIES\n");
		for (ConnectivityFaces cf : connectivityF) {
			out.append(getFaceName(cf.getFace1()) + " , "
					+ getFaceName(cf.getFace2()) + " , COHESION\n");
		}
		out.append("*End Assembly\n");
		out.append("**\n" + "** STEP: InitialStep\n" + "**\n"
				+ "*Step, name=InitialStep, nlgeom=YES\n"
				+ "*Dynamic, Explicit\n" + ", 1.\n" + "*Bulk Viscosity\n"
				+ "0.06, 1.2\n" + "** \n" + "** OUTPUT REQUESTS\n" + "** \n"
				+ "*Restart, write, number interval=1, time marks=NO\n"
				+ "** \n" + "** FIELD OUTPUT: FieldOutput\n" + "** \n"
				+ "*Output, field, variable=PRESELECT\n" + "** \n"
				+ "** HISTORY OUTPUT: HistoryOutPut\n" + "** \n"
				+ "*Output, history, variable=PRESELECT\n" + "*End Step\n");

		return out.toString();
	}

	private static String getFaceName(Face fc) {
		return "El" + fc.getElementId() + "S" + fc.getElementFaceId();
	}

	private static int numberOfFaces(int nodeNumbler) {
		if (nodeNumbler == 4) {
			return 4;
		}
		if (nodeNumbler == 6) {
			return 5;
		}
		if (nodeNumbler == 10) {
			return 4;
		}
		return 0;
	}
}
