package ru.isc.mesh3du;

public class ConnectivityFaces {
	private Face face1;
	private Face face2;

	public ConnectivityFaces(Face f1, Face f2) {
		face1 = f1;
		face2 = f2;
	}

	Face getFace1() {
		return face1;
	}

	Face getFace2() {
		return face2;
	}

	@Override
	public String toString() {
		return "ConnectivityFaces [face1=" + face1 + "," + "\n" + "face2="
				+ face2 + "]" + "\n";
	}
}
