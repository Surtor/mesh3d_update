package ru.isc.mesh3du;

public final class NodeImp implements Node, Cloneable {
	private long id; // идентификатор
	private double x, y, z;// координаты

	public boolean isSameCoordinates(Node other) {
		return (x == other.getX() && y == other.getY() && z == other.getZ());
	}

	public NodeImp(long id, double x, double y, double z) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public NodeImp(Node nd) {
		this(nd.getId(), nd.getX(), nd.getY(), nd.getZ());
	}

	public NodeImp clone() {
		try {
			return (NodeImp) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "NodeImp [id=" + id + ", x=" + x + ", y=" + y + ", z=" + z + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeImp other = (NodeImp) obj;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#getX()
	 */
	@Override
	public double getX() {
		return x;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#setX(double)
	 */
	@Override
	public void setX(double x) {
		this.x = x;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#setId(long)
	 */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#getY()
	 */
	@Override
	public double getY() {
		return y;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#setY(double)
	 */
	@Override
	public void setY(double y) {
		this.y = y;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#getZ()
	 */
	@Override
	public double getZ() {
		return z;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ru.isc.mesh3du.Node#setZ(double)
	 */
	@Override
	public void setZ(double z) {
		this.z = z;
	}

}
