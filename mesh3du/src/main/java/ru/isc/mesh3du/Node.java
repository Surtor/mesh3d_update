package ru.isc.mesh3du;

public interface Node {

	public abstract double getX();

	public abstract void setX(double x);

	public abstract long getId();

	public abstract void setId(long id);

	public abstract double getY();

	public abstract void setY(double y);

	public abstract double getZ();

	public abstract void setZ(double z);

	public abstract boolean isSameCoordinates(Node other);

	public abstract Node clone();

}