package ru.isc.mesh3du;

import java.io.FileNotFoundException;
import java.util.Iterator;

import ru.isc.mesh3du.exceptions.Mesh3dUExceptions;
import ru.isc.mesh3du.exceptions.NothingToSaveException;

public class Mesh3du {
	public static void main(String[] args) {

		MeshFile meshFile = new MeshFile();
		try {
			meshFile.loadFromFile("a.inp");

		} catch (Mesh3dUExceptions e) {
			System.out.print(e.getErrorMessage());
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.out.print("Input file doesn't exist!");
			System.exit(2);
		}

		// Используем разные алгоритмы в зависимости от количества элементов
		Iterator<Element> it = meshFile.getMesh().getElementIterator();
		if (!it.hasNext()) {
			System.out.println("Input file doesn't have elements");
			return;
		}
		int countElementNodes = it.next().quantityNodes();

		if (countElementNodes == 6) {
			// Для клиньев
			// MeshUtilities.divideAllElements(meshFile.getMesh());
			// MeshUtilities.shrinkWedge(meshFile.getMesh());
			MeshUtilities.addCohesiveWedge(meshFile.getMesh());
		}
		if (countElementNodes == 4) {
			// Для тетрайдеров добавление когезионных элементов
			// MeshUtilities.divideAllElements(meshFile.getMesh());
			MeshUtilities.addCohesiveTetraider(meshFile.getMesh());
			// MeshUtilities.shrinkTetraiderTypeEl(meshFile.getMesh(), 0.1,
			// "C3D4");
		}

		/*
		 * if (countElementNodes == 4) { // Для тетрайдоров добавление
		 * когензионных связей
		 * MeshUtilities.addCohesiveInteractionsTetraider(meshFile); } if
		 * (countElementNodes == 6) { // Для клиньев добавление когензионных
		 * связей MeshUtilities.addCohesiveInteractionsWedge(meshFile); } if
		 * (countElementNodes == 10) { // Для тетрайдеров второго порядка
		 * добавление когензионных связей
		 * MeshUtilities.addCohesiveInteractionsSecondTetra(meshFile); }
		 */
		try {
			meshFile.saveToFile("aout.inp");
		} catch (NothingToSaveException e) {
			// TODO Auto-generated catch block
			System.out.print(e.getErrorMessage());
			System.exit(1);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.print("Can't create output file!");
			System.exit(2);
		}

		/*
		 * System.out.println(inFile.next()); inFile.close();
		 */

		/*
		 * Mesh testMesh = new Mesh(); Node a1 = new NodeImp(1, 1, 1, 1); Node
		 * a2 = new NodeImp(2, 2, 2, 2); Node a3 = new NodeImp(3, 3, 3, 3); Node
		 * a4 = new NodeImp(4, 4, 4, 4); Node a5 = new NodeImp(5, 5, 5, 5); Node
		 * a6 = new NodeImp(6, 6, 6, 6); Node a7 = new NodeImp(7, 7, 7, 7); Node
		 * a8 = new NodeImp(10, 8, 8, 8);
		 * 
		 * testMesh.addNewElement(new ElementImp(0, a1, a2, a3, a4));
		 * testMesh.addNewElement(new ElementImp(0, a1, a2, a3, a5));
		 * testMesh.addNewElement(new ElementImp(0, a1, a2, a3, a6));
		 * testMesh.addNewElement(new ElementImp(0, a1, a2, a3, a7));
		 * testMesh.addNewElement(new ElementImp(0, a1, a2, a3, a8));
		 * testMesh.addElement(new ElementImp(10, a1, a2, a5, a8));
		 * testMesh.addNewElement(new ElementImp(0, a1, a3, a5, a8));
		 * 
		 * System.out.println(testMesh.removeElement(3));
		 * 
		 * testMesh.getElementIterator().forEachRemaining( value ->
		 * System.out.println(value));
		 */

	}
}
