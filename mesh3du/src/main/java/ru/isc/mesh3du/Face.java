package ru.isc.mesh3du;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Face implements Iterable<Node> {
	private int elementId;
	private List<Node> nodes = new ArrayList<>();
	private int elementFaceId;

	public boolean isNeighbor(Face other) {
		Set<Node> thisEl = new HashSet<>(nodes);
		Set<Node> otherEl = new HashSet<>(other.nodes);
		return thisEl.equals(otherEl);
	}

	public Face() {
	}

	public Face(int elementId) {
		this.elementId = elementId;
	}

	public int getElementFaceId() {
		return elementFaceId;
	}

	public void setElementFaceId(int elementFaceId) {
		this.elementFaceId = elementFaceId;
	}

	public int getElementId() {
		return elementId;
	}

	public void setElementId(int id) {
		elementId = id;
	}

	public void addNode(Node nd) {
		nodes.add(nd);
	}

	public void clearAllNodes() {
		nodes.clear();
	}

	public boolean changeNode(int i, Node nd) {
		/*
		 * if (i > (nodes.size())) { return false; }
		 */
		nodes.set(i, nd);
		return true;
	}

	public Iterator<Node> iterator() {
		return nodes.iterator();
	}

	@Override
	public String toString() {
		return "Face [elementId=" + elementId + ", nodes=" + nodes + "]";
	}

}
